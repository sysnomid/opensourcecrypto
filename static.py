from flask_frozen import Freezer
from app import app

static_runner = Freezer(app)

if __name__ == '__main__':
    static_runner.freeze()
